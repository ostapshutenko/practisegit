package com.example.wikihero;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.webkit.WebView;

public class Activity2 extends AppCompatActivity {
    private WebView web;
    private TextView looklog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
       // looklog = findViewById(R.id.textViewLookLogin);
        Bundle arguments = getIntent().getExtras();
        //String lin = ;
        web = findViewById(R.id.VebViewWeb);
        web.setWebViewClient(new WebViewClient());
        // включаем поддержку JavaScript
        web.getSettings().setJavaScriptEnabled(true);
        //web.setWebViewClient(WebViewClient());
        // указываем страницу загрузки
        //web.loadUrl(arguments.get("LINK").toString());


    }
    @Override
    protected void onStart() {
        super.onStart();
        Bundle arguments = getIntent().getExtras();
        setTitle(arguments.get("LOGIN").toString());
        // указываем страницу загрузки
        web.loadUrl(arguments.get("LINK").toString());
    }

   /* public void onCliclBack(View view) {
        this.onStop();
        Intent MainActivityIntent = new Intent("com.example.wikihero.MainActivity");
        startActivity(MainActivityIntent);
    }*/
}