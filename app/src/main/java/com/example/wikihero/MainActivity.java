package com.example.wikihero;
/*
some
good
commetary
*/
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

    //private String[] logins = getResources().getStringArray(R.array.Logins);
    //private String[] passwords = getResources().getStringArray(R.array.Passwords);
    //private String[] links = getResources().getStringArray(R.array.Links);
    private Button ButLogin;
    private TextView Views_1, Views_2, Views_3, Views_4;
    private String log;
    private String pas;
    private String lin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButLogin = findViewById(R.id.buttonLogin);
        Views_1 = findViewById(R.id.textViewLogin);
        Views_2 = findViewById(R.id.textViewPassword);
        Views_3 = findViewById(R.id.textViewLabelConfirm);
        Views_4 = findViewById(R.id.textView);
    }

    public void onClickLogin(View view) {

        String[] logins = getResources().getStringArray(R.array.Logins);
        String[] passwords = getResources().getStringArray(R.array.Passwords);
        String[] links = getResources().getStringArray(R.array.Links);

        //lin = "Password Error";
        lin = "";
        String errors = "autentification error.";
        log = Views_1.getText().toString();
        pas = Views_2.getText().toString();


        for(int i = 0; i < logins.length; i++) {
            if (logins[i].equals( log)  && passwords[i].equals(pas)) {
                lin = links[i];
                //lin = "ok";
            }
        }
        //lin = Views_3.getText().toString();
        if(lin.equals("")) {
            Views_3.setText(errors);

        }
        else {
            Views_3.setText("");
            Intent WevActivityIntent = new Intent("com.example.wikihero.Activity2");
            WevActivityIntent.putExtra("LINK", lin);
            WevActivityIntent.putExtra("LOGIN", log);
            startActivity(WevActivityIntent);
        }
    }
}